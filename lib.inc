section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov eax, 60
  syscall



; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
  mov rsi, rdi
  call string_length
  mov rdx, rax
  mov rdi, 1
  mov rax, 1
  syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
  xor rax, rax
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  mov rdi, 0xA
  push rcx
  call print_char
  pop rcx
  ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rcx, 0
    mov r9, 10
    dec rsp
    mov [rsp], al
    mov rax, rdi
    .loop:
        xor rdx, rdx
        div r9
        add dl, '0'
        dec rsp
        mov [rsp], dl
        inc rcx
        cmp rax, 0
        jne .loop
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    inc rsp
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
  xor rdx, rdx
  mov r8, 0
  mov r9, 10
  mov rax, rdi
  cmp rax, 0
  jge .print
  push rax
  push rdx
  push r8
  push r9
  push rcx
  mov rdi, '-'
  call print_char
  pop rcx
  pop r9
  pop r8
  pop rdx
  pop rax
  neg rax
  .print:
    mov rdi, rax
    push rcx
    call print_uint
    pop rcx
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
  push rbx
  xor rbx, rbx
    xor rax, rax
  .loop:
    mov cl, byte [rdi+rbx]
    cmp cl, byte [rsi+rbx]
    jnz .not_ok
    cmp byte [rdi + rbx], 0
    jz .ok
    inc rbx
    jmp .loop
  .ok:
    mov rax, 1
    jmp .end
  .not_ok:
    mov rax, 0
    jmp .end
  .end:
    pop rbx
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
  push rdi
  push rsi
  push rdx
  push 0
    xor rax, rax
  mov rdi, 0
  mov rsi, rsp
  mov rdx, 1
  syscall
  mov rax, [rsp]
    pop rdx
  pop rdx
  pop rsi
  pop rdi
  ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
  mov r8, rdi
  mov r9, rsi
  .avoid_spaces:
    call read_char
    cmp al, 0x20
    je .avoid_spaces
    cmp al, 0x9
    je .avoid_spaces
    cmp al, 0xA
    je .avoid_spaces
  xor rdx, rdx
  .loop:
    cmp al, 0xA
    je .finish
    cmp al, 0x20
    je .finish
    cmp al, 4
    je .finish
    cmp al, 0x9
    je .finish
    cmp al, 0
    je .finish
    inc rdx
    cmp rdx, r9
    jge .overflow
    dec rdx
    mov [r8+rdx], al
    inc rdx
    push rdx
    push r8
    call read_char
    pop r8
    pop rdx
    jmp .loop
  .finish:
    mov byte [r8+rdx], 0
    mov rax, r8
    ret
  .overflow:
    xor rax, rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  xor r8, r8
  xor rax, rax
  xor rdx, rdx
  xor rsi, rsi
  mov r9, 10
  .loop:
    mov sil, [rdi+r8]
    cmp sil, '0'
    jl .finish
    cmp sil, '9'
    jg .finish
    inc r8
    sub sil, '0'
    mul r9
    add rax, rsi
    jmp .loop
  .finish:
    mov rdx, r8
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    ret
  .neg:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  xor rcx, rcx
  xor r8, r8
  call string_length
  cmp rax, rdx
  jle .loop
  xor rax, rax
  ret
  .loop:
    mov r8b, [rdi+rcx]
    mov [rsi+rcx], r8b
    inc rcx
    cmp byte[rsi+rcx], 0
    jne .loop
  mov rax, rdx
  ret
